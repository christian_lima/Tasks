// const variables which will be needed for the functions later in the doc
const readline = require('readline');
const os = require('os');
const fs = require('fs');
const http = require('http');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


const question = `Type in 1, 2, or 3 in the terminal to select one of the following options:\n1 - Read 'package.json' file\n2 - Display OS Information\n3 - Start HTTP-server\nType a number input: `;
rl.question(question, enteredInput => {
    enteredInput = parseInt(enteredInput);
    switch (enteredInput) {
        case 1:
            readPackageJSON();
            break;
        case 2:
            displayOSInfo();
            break;
        case 3:
            startHTTPServer();
            break;
        default:
            rl.write("Invalid input. Valid inputs are: 1, 2, or 3. CTRL-C to exit and try again with 'node index.js input'.");
    }

});

// Function which selects 'package.json' file, and writes the content into the console
function readPackageJSON() {
    rl.write('Reading package.json-file');
    fs.readFile('./package.json', 'utf-8', (error, data) => {
        if (error) rl.write(error);
        else rl.write(data);
        rl.close();
    });

}

// Function to read  OS Information and write it into the console 
function displayOSInfo() {
    rl.write('Operating System information:\n');

    // stores OS-variables
    const totalmem = 'SYSTEM MEMORY: ' + ('SYSTEM MEMORY ', (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB');
    const freemem = 'FREE MEMORY ' + ('FREE MEMORY ', (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + ' GB');
    const CPUCores = 'CPU CORES: ' + ('CPU CORES: ', (os.cpus().length));
    const arch = 'ARCH: ' + os.arch();
    const platform = 'PLATFORM: ' + os.platform();
    const release = 'RELEASE: ' + os.release;
    const user = 'USER: ' + os.userInfo().username;

    // Output
    rl.write(totalmem + '\n' + freemem + '\n' + CPUCores + '\n' + arch + '\n' + platform + '\n' + release + '\n' + user + '\n');
    rl.close();
}

// Function to start a HTTP-server, using port: 3000 (type in URL:localhost:3000 to test output in browser);
function startHTTPServer() {
    rl.write('Starting the HTTP server...\n');
    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World! ');
    });
    rl.write('Listening on; port 3000.... \n');
    server.listen(3000);
    rl.close();
}